// Simple web/websocket client
// (c) 2018, Reto Bättig
//
// Implements simple chat application with the simple web socket server
// All the data in the "model" structure is synchronized with all other clients
//
// The demo implements a simple chat application

var model; // The global data model which gets synchronized with all other clients
           // Call mySocketConnection.updateModel whenever the model has changed locally
           // and you want to synchronize it with the other clients


// Logs messages to the DOM and the console
function Log(s) {
    document.getElementById("log").innerHTML += s+"<br>\n";
    console.log(s);
}

// Updates the chat messages, gets called when the model gets updated
function RenderModel() {
    if (typeof model.messages == 'undefined') {
        return
    }

    // only print the last 10 messages
    var s="<div class='messagehead'>Chat history</div>\n";
    var len = model.messages.length;
    for (var i=0; i<Math.min(10, len); i++) {
        s+="<div class='message'>"+model.messages[len-i-1]+"</div>\n";
    }
    document.getElementById("model").innerHTML = s;
}

// Button handler, adds string to model and updates model on all clents
function AddString(event) {
    event.preventDefault();
    if (typeof model.messages == 'undefined') {
        model.messages=[];
    }
    var newMessage = document.getElementById("myInput").value;
    document.getElementById("myInput").value="";
    if (newMessage.length > 0) {
        model.messages.push(newMessage);
        mySocketConnection.updateModel();
    }
}

function attachButtonHandler() {
    var form = document.getElementById("inputform");
    form.addEventListener('submit', AddString);
}

// Socket connection handler function. Does not need to be changed
// returns only one function:
// mySocketConnection.updateModel()
function InitWebSocketHandler() {

    var ws=false; // the WebSocket connection handler

    function closeWebsockets() {
        if (ws) {
            try {
                ws.close();
            } catch(err) {
                Log("Websocket already closed");
            }
        }
    }

    function initWebsockets() {
        Log("Connecting websocket to "+window.location.host);

        closeWebsockets();

        ws = new WebSocket("ws://"+window.location.host);

        // Handlers
        ws.onopen = function () {
            Log("Websocket open")
            ws.send(JSON.stringify({command: "getdata"}));
        };

        ws.onerror = function (error) {
            Log('WebSocket Error ' + error);
        };

        ws.onclose = function() {
            Log('Websocket Closed');
        };

        // Log messages from the server
        ws.onmessage = function (e) {
            model=JSON.parse(e.data);
            RenderModel();
        };
    }

    // Checks Websockets and reconnects if no connection is available
    function checkWebsockets() {
        try {
            if (ws.readyState == 0 || ws.readyState == 1) {
                // WS is in connecting or open state. Everything ok
                return 0;
            }
        } catch(err) {}

        // WS is not in connectiong or open state
        Log("Connection lost, restarting Websockets")
        RenderModel();
        initWebsockets();
    }

    // Repeat cheching websocket connection every 5 seconds
    window.setInterval(checkWebsockets, 5000);

    // Init the WebSockets for the first time
    initWebsockets();

    // Return the interface
    return {
        updateModel : function() {
            ws.send(JSON.stringify({command: "setdata", model: model}));
        }
    }
};

//initialize page
Log("Logfile:");
attachButtonHandler();
var mySocketConnection = InitWebSocketHandler();

