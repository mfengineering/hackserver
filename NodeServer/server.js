// Simple web server and websocket server
// (c) 2018, Reto Bättig
//
// The web server serves static files from the directory "./public"
// the web socket server synchronizes the data structure "model" with all connected clients
// the web socket server expects a json structure and knows 2 commands:
//      {command: "getdata"}
//      {command: "setdata", model: {... arbitrary data model which gets distributed to all clients }}


var express = require('express');
var app = express();
var expressWs = require('express-ws')(app);
const normalizePort = require('normalize-port');

// Data Model of the application. Gets distributed to all connected clients via WebSockets
var model = {
    appname : "Dummy NodeServer"
};

function HandleWsMessage(ws, msg) {
    //console.log(msg);
    var message = JSON.parse(msg);
    switch (message.command) {
        case 'getdata':
            break;
        case 'setdata':
            model = message.model;
            break;
        default:
            break;
    }
    WebSocketBroadcast(model);
}

// Static Web Server
app.use(express.static('public'));

// Web Socket handling
var clients=[]; // list of all connected clients at the moment

app.ws('/', function(ws, req) {
    console.log('got connection from: '+req.connection.remoteAddress);
    clients.push(ws); // add new client to list of connected clients
    WebSocketBroadcast(model); // update all clients
    console.log("Added client to list. NumClients="+clients.length);

    // Install handlers for new web socket connection
    ws.on('close', function(msg) {
        console.log('connection closed to: '+req.connection.remoteAddress);
        clients.splice(clients.indexOf(ws),1); // remove client from list of connected clients
        console.log("Deletet client form list. NumClients="+clients.length);
    });
    ws.on('error', function(msg) {
        console.log('connection error from: '+req.connection.remoteAddress+' '+msg);
    });
    ws.on('message', function(msg) {
        HandleWsMessage(ws, msg);
    });
});

// Broadcast data model to all connected clients
function WebSocketBroadcast(msg) {
    clients.forEach(function(ws) {
        ws.send(JSON.stringify(msg));
    })
}

// Start Web Server
var port = normalizePort(process.env.PORT || '3000');
app.listen(port, function () {
    console.log('Web Server listening on port '+port);
});